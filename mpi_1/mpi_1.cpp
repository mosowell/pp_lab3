﻿#include <iostream> 
#include "mpi.h"
#include <math.h>
#include <iomanip>

using namespace std; 

int main() {
	MPI_Init(NULL, NULL); 

	int procNum, procRank;
	long long iterations = 1000000000;

	double startTime = MPI_Wtime();

	MPI_Comm_size(MPI_COMM_WORLD, &procNum); 
	MPI_Comm_rank(MPI_COMM_WORLD, &procRank); 

	double processSum = 0.0;

	for (int i = procRank; i < iterations; i += procNum) {
		processSum += (i % 2 == 0 ? 1.0 : -1.0)  *  1.0 / (2.0 * i + 1.0);
	}

	double calculatedPi = 0.0;
	MPI_Reduce(&processSum, &calculatedPi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD); 
	calculatedPi *= 4.0;
	double endTime = MPI_Wtime();

	MPI_Finalize(); 

	if (procRank == 0) {
		double pi = 3.1415926535897932384626433832795;

		cout << "Number of iterations = " << iterations << endl;
		printf("Calculated value of Pi = %3.20f\n", calculatedPi); // С cout почему то не выводит всё дробную часть
		printf("Refrence value of Pi = %3.20f\n", pi); 
		printf("Calculation error = %3.20f\n", abs(calculatedPi - pi)); 
		cout << "Computing time = " << endTime - startTime << " seconds" << endl;
	}
}